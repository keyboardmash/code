import psycopg2

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from helper import log


class Database:

    def __init__(self, name):
        self.connection = psycopg2.connect(host='database', user='postgres')
        self.connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.cursor = self.connection.cursor()

        self._check_object('database', name)
        self.name = name

        # We need to create a new connection in order to change databases.
        self.cursor.close()
        self.connection.close()
        self.connection = psycopg2.connect(host='database', user='postgres', database=name)
        self.connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.cursor = self.connection.cursor()

        self._check_object('table', 'users', "("
                                             "    id SERIAL PRIMARY KEY,"
                                             "    name VARCHAR(20) UNIQUE NOT NULL,"
                                             "    cookie VARCHAR(256) UNIQUE NOT NULL,"
                                             "    games_completed INTEGER NOT NULL DEFAULT 0,"
                                             "    games_won INTEGER NOT NULL DEFAULT 0,"
                                             "    rounds_completed INTEGER NOT NULL DEFAULT 0,"
                                             "    rounds_judged INTEGER NOT NULL DEFAULT 0,"
                                             "    rounds_won INTEGER NOT NULL DEFAULT 0"
                                             ")")
        if not self._check_object('table', 'topics', "("
                                                     "    id SERIAL PRIMARY KEY,"
                                                     "    text VARCHAR(256) UNIQUE NOT NULL"
                                                     ")"):
            topics_file = open('topics.txt')
            for topic in topics_file:
                self.cursor.execute('INSERT INTO topics (text) VALUES (%s)', (topic.strip(),))
            topics_file.close()

        if not self._check_object('table', 'actions', "("
                                                      "    id SERIAL PRIMARY KEY,"
                                                      "    text VARCHAR(256) UNIQUE NOT NULL"
                                                      ")"):
            actions_file = open('actions.txt')
            for action in actions_file:
                self.cursor.execute('INSERT INTO actions (text) VALUES (%s)', (action.strip(),))
            actions_file.close()

    def _check_object(self, catalog_name, name, schema=''):
        log('Checking if the {} {} exists...'.format(catalog_name, name))

        column_name = 'datname' if catalog_name == 'database' else 'tablename'
        table_name = 'pg_database' if catalog_name == 'database' else 'pg_tables'

        # It is normally a really bad idea to use a ".format()" call in an SQL query, but this is an
        # exception because psycopg2 will put quotes around the parameters which we do not want.
        # This however still does not present a security risk, because the parameters are defined by
        # us.
        self.cursor.execute("SELECT {column_name} "
                            "FROM pg_catalog.{table_name} "
                            "WHERE {column_name} = '{name}'".format(table_name=table_name,
                                                                    column_name=column_name,
                                                                    name=name))

        if self.cursor.fetchone():
            log('The {} {} exists!'.format(catalog_name, name))
            return True

        log('The {} {} does not exist, creating one now...'.format(catalog_name, name))

        # It is normally a really bad idea to use a ".format()" call in an SQL query, but this
        # is an exception because psycopg2 will put quotes around the parameters which we do not
        # want. This however still does not present a security risk, because the parameters are
        # defined by us.
        self.cursor.execute("CREATE {} {} {}".format(catalog_name, name, schema))

        log('The {} {} has been created!'.format(catalog_name, name))
        return False

    def _delete(self):
        # We need to set up a new connection that is not specific to a database in order to delete
        # the database.
        self.cursor.close()
        self.connection.close()
        self.connection = psycopg2.connect(host='database', user='postgres')
        self.connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.cursor = self.connection.cursor()

        self.cursor.execute("DROP DATABASE {}".format(self.name))

    def register_user(self, name, cookie):
        log('Registering user "{}"...'.format(name))

        # First check if the username is too long.
        if len(name) > 20:
            return -1

        self.cursor.execute("SELECT * "
                            "FROM users "
                            "WHERE name = %s", (name,))

        # Second, check if the username has already been taken.
        if self.cursor.fetchone():
            return -2

        # Finally add the user.
        self.cursor.execute("INSERT INTO users (name, cookie) "
                            "VALUES (%s, %s)", (name, cookie))
        log('User "{}" registered.'.format(name))

        return True

    def get_user_raw(self, cookie):
        self.cursor.execute("SELECT * "
                            "FROM users "
                            "WHERE cookie = %s", (cookie,))

        return self.cursor.fetchone()

    def get_random_topics(self, number):
        self.cursor.execute("SELECT text "
                            "FROM topics "
                            "ORDER BY random() "
                            "LIMIT %s", (number,))

        return [item[0] for item in self.cursor.fetchall()]

    def get_random_actions(self, number):
        self.cursor.execute("SELECT text "
                            "FROM actions "
                            "ORDER BY random() "
                            "LIMIT %s", (number,))

        return [item[0] for item in self.cursor.fetchall()]

    def add_game_completed(self, cookies):
        self.cursor.execute("UPDATE users "
                            "SET games_completed = games_completed + 1 "
                            "WHERE cookie IN %s", (tuple(cookies),))

    def add_game_won(self, cookie):
        self.cursor.execute("UPDATE users "
                            "SET games_won = games_won + 1 "
                            "WHERE cookie = %s", (cookie,))

    def add_round_completed(self, cookies):
        self.cursor.execute("UPDATE users "
                            "SET rounds_completed = rounds_completed + 1 "
                            "WHERE cookie IN %s", (tuple(cookies),))

    def add_round_judged(self, cookie):
        self.cursor.execute("UPDATE users "
                            "SET rounds_judged = rounds_judged + 1 "
                            "WHERE cookie = %s", (cookie,))

    def add_round_won(self, cookie):
        self.cursor.execute("UPDATE users "
                            "SET rounds_won = rounds_won + 1 "
                            "WHERE cookie = %s", (cookie,))
