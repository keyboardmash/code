from flask import Flask, render_template, request, redirect, make_response
from flask_socketio import SocketIO, join_room, leave_room

from database_helper import Database
from helper import generate_session_cookie, get_user_if_good, log, get_error_code, get_error_message
from game import Game, get_game, clean_games
from config import conf

app = Flask(__name__)
socket = SocketIO(app)
database = Database(conf['database_name'])


@app.route('/')
def index_route():
    if get_user_if_good(request, database):
        return redirect('/menu')

    return render_template('index.html')


@app.route('/error')
def error_route():
    error_code = request.args['error_code']
    return render_template('error.html', error=get_error_message(error_code))


@app.route('/register-user', methods=['POST'])
def register_user_route():
    username = request.form.get('username')
    cookie = generate_session_cookie()

    if username:
        error = database.register_user(username, cookie)
        if error == -1:
            return redirect('/error?error_code={}'.format(get_error_code('USERNAME_TOO_LONG')))
        elif error == -2:
            return redirect('/error?error_code={}'.format(get_error_code('USERNAME_EXISTS')))

        response = make_response(redirect('/menu'))
        response.set_cookie('session', cookie, max_age=9999999999)
        return response

    return redirect('/')


@app.route('/menu')
def menu_route():
    user = get_user_if_good(request, database)

    # Always check that the user is signed in. If not, redirect them back to the login page.
    if not user:
        return redirect('/')

    return render_template('menu.html', user=user)


@app.route('/create-game')
def create_game_route():
    user = get_user_if_good(request, database)

    # Always check that the user is signed in. If not, redirect them back to the login page.
    if not user:
        return redirect('/')

    # We need to clean out the empty games every once in a while, so a good place for this call is
    # whenever a game is created.
    clean_games()

    game = Game(user, socket, database)

    log('Game created with code {}.'.format(game.code))

    return redirect('/game?code={}'.format(game.code))


@app.route('/game')
def game_route():
    user = get_user_if_good(request, database)

    # Always check that the user is signed in. If not, redirect them back to the login page.
    if not user:
        return redirect('/')

    code = request.args.get('code')
    if not code:
        return redirect('/error?error_code={}'.format(get_error_code('GAME_DOES_NOT_EXIST')))

    game = get_game(code)
    if not game:
        return redirect('/error?error_code={}'.format(get_error_code('GAME_DOES_NOT_EXIST')))

    if not game.get_game_user(user['cookie']):
        error = game.add_user(user)
        if error:
            return redirect('/error?error_code={}'.format(get_error_code(error)))

        socket.emit('user_join', user['name'], room=code)

    response = make_response(render_template('game.html', user=user, code=code, game=game))
    response.set_cookie('code', code)
    return response


@app.route('/leave')
def leave_route():
    user = get_user_if_good(request, database)

    # Always check that the user is signed in. If not, redirect them back to the login page.
    if not user:
        return redirect('/')

    # We don't care what happens if the code is invalid, since all the user wants to do is leave.
    code = request.cookies.get('code')
    if code:
        game = get_game(code)
        if game:
            game.remove_user(user)

    return redirect('/menu')


@socket.on('connect')
def connect_on():
    cookie = request.cookies.get('session')
    code = request.cookies.get('code')
    if not cookie or not code:
        return
    game = get_game(code)
    if not game:
        return
    if not game.user_connect(cookie):
        return

    join_room(code)

    user = get_user_if_good(request, database)
    log('User {} connected to game {}.'.format(user['name'], code))


@socket.on('disconnect')
def disconnect_on():
    cookie = request.cookies.get('session')
    code = request.cookies.get('code')

    if not cookie or not code:
        return
    game = get_game(code)
    if not game:
        return
    if not game.user_disconnect(cookie):
        return

    leave_room(code)

    user = get_user_if_good(request, database)
    log('User {} disconnected from game {}.'.format(user['name'], code))


@socket.on('start_game')
def start_game_on():
    cookie = request.cookies.get('session')
    code = request.cookies.get('code')
    if not cookie or not code:
        return
    game = get_game(code)
    if not game:
        return

    # We have to make sure the start game request is coming from the admin of the game.
    if game.is_admin({'cookie': cookie}):
        game.start()

    log('Game {} has started!'.format(code))


@socket.on('submit')
def submit_on(submission):
    cookie = request.cookies.get('session')
    code = request.cookies.get('code')
    if not cookie or not code:
        return
    game = get_game(code)
    if not game:
        return

    game.submit(cookie, submission)

    user = get_user_if_good(request, database)
    log('User {} has submitted {} to game {}.'.format(user['name'], submission, code))


@socket.on('kick')
def kick_on(name):
    cookie = request.cookies.get('session')
    code = request.cookies.get('code')
    if not cookie or not code:
        return
    game = get_game(code)
    if not game:
        return

    # We have to make sure the kick request is coming from the admin of the game.
    if game.is_admin({'cookie': cookie}):
        game.kick(name)

    log('User {} has been kicked from game {}.'.format(name, code))


@socket.on('ban')
def ban_on(name):
    cookie = request.cookies.get('session')
    code = request.cookies.get('code')
    if not cookie or not code:
        return
    game = get_game(code)
    if not game:
        return

    # We have to make sure the ban request is coming from the admin of the game.
    if game.is_admin({'cookie': cookie}):
        game.ban(name)

    log('User {} has been banned from game {}.'.format(name, code))


@socket.on('close')
def close_on():
    """ Closes submissions for the current round. This event should be called from the admin after
        a certain about of time after submission opens. This is a hack, and should really be handled
        server-side. However, socket-io has very specific requirements in order to fire events from
        background processes, and it wasn't worth it for our project.
    """
    cookie = request.cookies.get('session')
    code = request.cookies.get('code')
    if not cookie or not code:
        return
    game = get_game(code)
    if not game:
        return

    # We have to make sure the ban request is coming from the admin of the game.
    if game.is_admin({'cookie': cookie}):
        game.close()

    user = get_user_if_good(request, database)
    log('User {} has closed the polls on game {}.'.format(user['name'], code))


if __name__ == '__main__':
    socket.run(app, host='0.0.0.0', port=80)
