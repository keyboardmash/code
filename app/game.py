import random

from helper import generate_game_code, log, get_error_code
from config import conf

games = []


def get_game(code):
    """ Get the Game object from its game code.

    :param code: The 6 digit game code.
    :type code: str
    :return: The Game object, or None if the game is not found.
    :rtype: Game
    """
    for game in games:
        if game.code == code:
            return game
    return None


def clean_games():
    log('Current games before: {}'.format(len(games)))
    for game in games:
        if all([not user.connected for user in game.game_users]):
            games.remove(game)
    log('Current games after: {}'.format(len(games)))


class Game:
    def __init__(self, admin, socket, database):
        # Keep generating codes until we find one that is unique.
        self.code = generate_game_code()
        while get_game(self.code):
            self.code = generate_game_code()

        self.admin = self._make_game_user(admin)
        self.socket = socket
        self.database = database
        self.game_users = []
        self.banned_users = []
        self.judge_index = None
        self.topic = None
        self.story = []
        self.choices = []

        # Game States:
        #   0 = Waiting For Game Start
        #   1 = Voting On Topic
        #   2 = Judge Picking Action
        #   3 = Players Submitting Sentences
        #   4 = Judge Picking Sentence
        #   5 = End Game
        self.state = 0

        games.append(self)

    def _make_game_user(self, user):
        return self.GameUser(user['name'], user['cookie'])

    def _get_user_by_submission(self, submission, exclude_judge=False):
        for game_user in self.game_users:
            if game_user.submission == submission:
                if not exclude_judge or not self.game_users[self.judge_index] == game_user:
                    return game_user
        return None

    def _check_submissions(self):
        if self.state == 1:
            # If the state is "Voting On Topic", we need submission from all players.
            submissions = [game_user.submission for game_user in self.game_users]
            if all(submissions):
                # Choose the most voted submission
                self.topic = max(set(submissions), key=submissions.count)

                self._start_round()

        elif self.state == 2:
            self.state = 3
            self.action = self.game_users[self.judge_index].submission
            self.game_users[self.judge_index].submission = None
            self.socket.emit('open_sentence_submissions', self.action, room=self.code)

        elif self.state == 3:
            self.choices = [game_user.submission for game_user in self.game_users
                            if not game_user == self.game_users[self.judge_index]]

            if all(self.choices):
                log('All non-judge players have submitted sentences!')
                random.shuffle(self.choices)
                self.socket.emit('open_judging', self.choices, room=self.code)
                self.state = 4

        elif self.state == 4:
            if self.game_users[self.judge_index].submission:
                log('Judge picked {}.'.format(self.game_users[self.judge_index].submission))
                winner = self._get_user_by_submission(self.game_users[self.judge_index].submission,
                                                      exclude_judge=True)
                winner.score += 1
                self.story.append(winner.submission)

                self.database.add_round_completed([game_user.cookie
                                                   for game_user in self.game_users])
                self.database.add_round_judged(self.game_users[self.judge_index].cookie)
                self.database.add_round_won(winner.cookie)

                if winner.score >= conf['score_to_win']:
                    self._end_game(winner)
                else:
                    self.socket.emit('end_round', (winner.name, winner.submission), room=self.code)
                    self._start_round()

    def _start_round(self):
        self.state = 2
        self.judge_index = random.randint(0, len(self.game_users) - 1)

        self.choices = self.database.get_random_actions(conf['number_of_action_choices'])

        for game_user in self.game_users:
            game_user.submission = None

        self.socket.emit('start_round',
                         (self.game_users[self.judge_index].name, self.topic, self.choices),
                         room=self.code)

    def _end_game(self, winner):
        log('{} won the game {}!'.format(winner.name, self.code))
        self.socket.emit('end_game', (winner.name, winner.submission), room=self.code)
        games.remove(self)
        self.database.add_game_completed([game_user.cookie for game_user in self.game_users])
        self.database.add_game_won(winner.cookie)

    def _check_for_not_enough_players(self):
        if len(self.game_users) == 1 and not self.state == 0:
            self.socket.emit('user_leave',
                             (self.game_users[0].name,
                              get_error_code('NOT_ENOUGH_PLAYERS')),
                             room=self.code)
            games.remove(self)
            return True
        return False

    def close(self):
        if self.state == 2 or self.state == 4:
            game_user = self.game_users[self.judge_index]
            log('Calling remove_user on {}'.format(game_user.name))
            self.remove_user({'cookie': game_user.cookie})
            return

        for game_user in self.game_users:
            if not game_user.submission:
                if not (self.state == 3 and game_user == self.game_users[self.judge_index]):
                    log('Calling remove_user on {}'.format(game_user.name))
                    self.remove_user({'cookie': game_user.cookie})
        self._check_submissions()

    def kick(self, name):
        self.remove_user({'cookie': self.get_game_user_by_name(name).cookie}, kicked_by_admin=True)

    def ban(self, name):
        # If a user is banned, it will appear to them as they have just been kicked. When they try
        # to rejoin, it will say the game does not exist. This is by design to make being banned
        # less hard on the feelings. ;(
        self.kick(name)
        self.banned_users.append(name)

    def start(self):
        self.state = 1

        if self._check_for_not_enough_players():
            return

        self.choices = self.database.get_random_topics(conf['number_of_topic_choices'])

        self.socket.emit('start', self.choices, room=self.code)

    def submit(self, cookie, submission):
        game_user = self.get_game_user(cookie)
        if game_user and len(submission) <= 100:
            game_user.submission = submission
            self._check_submissions()

    def is_admin(self, user):
        return user['cookie'] == self.admin.cookie

    def add_user(self, user):
        if len(self.game_users) >= conf['max_game_size']:
            return 'GAME_FULL'
        if user['name'] in self.banned_users:
            # If a user is banned and they try to rejoin, it will say the game does not exist. This
            # is by design to make being banned less hard on the feelings. ;(
            return 'GAME_DOES_NOT_EXIST'

        self.game_users.append(self._make_game_user(user))
        return 0

    def remove_user(self, user, kicked_by_admin=False):
        log('Trying to remove user!')
        for index, game_user in enumerate(self.game_users):
            if game_user.cookie == user['cookie']:

                # If the user being removed is the admin, kick everyone out.
                if game_user.cookie == self.admin.cookie:
                    log('User being ({}) removed is admin!'.format(game_user.name))

                    # Kick the admin for inactivity.
                    self.socket.emit('user_leave',
                                     (self.admin.name, get_error_code('KICKED_INACTIVITY')),
                                     room=self.code)
                    for game_user_to_kick in self.game_users:
                        # Kick all of the players because the admin has left.
                        self.socket.emit('user_leave', (game_user_to_kick.name, '7'),
                                         room=self.code)
                    games.remove(self)
                    return

                need_to_restart = False
                if self.judge_index:
                    if self.judge_index > index:
                        self.judge_index -= 1
                    elif self.judge_index == index:
                        need_to_restart = True
                self.game_users.remove(game_user)

                if kicked_by_admin:
                    self.socket.emit('user_leave', (game_user.name, get_error_code('KICKED')),
                                     room=self.code)
                else:
                    self.socket.emit('user_leave',
                                     (game_user.name, get_error_code('KICKED_INACTIVITY')),
                                     room=self.code)

                log('Removed user {}.'.format(game_user.name))

                if self._check_for_not_enough_players():
                    return

                if need_to_restart:
                    log('Need to restart because {} was the judge.'.format(game_user.name))
                    self._start_round()
                return

    def user_connect(self, cookie):
        game_user = self.get_game_user(cookie)
        game_user.connected = True
        return game_user

    def user_disconnect(self, cookie):
        game_user = self.get_game_user(cookie)

        if not game_user:
            return

        game_user.connected = False
        return game_user

    class GameUser:
        def __init__(self, name, cookie):
            self.name = name
            self.cookie = cookie

            self.connected = False
            self.submission = None
            self.score = 0

    def get_game_user_by_name(self, name):
        """ Get the GameUser object from its name.

        :param name: The users's name.
        :type name: str
        :return: The GameUser object, or None if the game user is not found.
        :rtype: GameUser
        """
        for user in self.game_users:
            if user.name == name:
                return user
        return None

    def get_game_user(self, cookie):
        """ Get the GameUser object from its cookie.

        :param cookie: The users's cookie.
        :type cookie: str
        :return: The GameUser object, or None if the game user is not found.
        :rtype: GameUser
        """
        for user in self.game_users:
            if user.cookie == cookie:
                return user
        return None
