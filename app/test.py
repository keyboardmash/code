from unittest import TestCase, main

from config import conf
from helper import get_error_code, get_error_message, generate_session_cookie, generate_game_code
from game import get_game, games
from database_helper import Database


class TestErrors(TestCase):

    def test_get_error_code(self):
        self.assertEqual(get_error_code('USERNAME_TOO_LONG'), '1')
        self.assertEqual(get_error_code('USERNAME_EXISTS'), '2')
        self.assertEqual(get_error_code('GAME_DOES_NOT_EXIST'), '3')
        self.assertEqual(get_error_code('GAME_FULL'), '4')
        self.assertEqual(get_error_code('KICKED_INACTIVITY'), '5')
        self.assertEqual(get_error_code('NOT_ENOUGH_PLAYERS'), '6')
        self.assertEqual(get_error_code('ADMIN_LEFT'), '7')
        self.assertEqual(get_error_code('KICKED'), '8')

    def test_get_error_message(self):
        self.assertEqual(get_error_message('1'), 'Username too long.')
        self.assertEqual(get_error_message('2'), 'Username already exists.')
        self.assertEqual(get_error_message('3'), 'Game does not exist.')
        self.assertEqual(get_error_message('4'), 'Game is full.')
        self.assertEqual(get_error_message('5'), 'You have been kicked due to inactivity.')
        self.assertEqual(get_error_message('6'), 'There are not enough players to '
                                                 'continue the game.')
        self.assertEqual(get_error_message('7'), 'The admin has left the game.')
        self.assertEqual(get_error_message('8'), 'You have been kicked by the admin.')


class TestGenerators(TestCase):

    def test_generate_session_cookie(self):
        self.assertEqual(len(generate_session_cookie()), 36)

    def test_generate_game_code(self):
        self.assertEqual(len(generate_game_code()), 6)
        self.assertTrue(generate_game_code().isdigit())


class TestGame(TestCase):

    class FakeGame:
        code = '123456'

    def test_get_game(self):
        game = self.FakeGame()
        games.append(game)
        self.assertEqual(get_game('123456'), game)


class TestDatabase(TestCase):

    def setUp(self):
        self.database = Database(conf['test_database_name'])

    def test_get_random_topics(self):
        self.assertEqual(len(self.database.get_random_topics(3)), 3)
        self.assertEqual(len(self.database.get_random_topics(5)), 5)

    def test_get_random_actions(self):
        self.assertEqual(len(self.database.get_random_actions(3)), 3)
        self.assertEqual(len(self.database.get_random_actions(5)), 5)

    def test_register_user(self):
        self.assertTrue(self.database.register_user('test_user', 'test_cookie'))
        self.assertEqual(self.database.register_user('test_user_name_that_is_too_long',
                                                     'test_cookie'), -1)

    def test_get_user_raw(self):
        self.database.register_user('test_user', 'test_cookie')
        user_raw = self.database.get_user_raw('test_cookie')
        self.assertEqual(user_raw[1], 'test_user')
        self.assertEqual(user_raw[2], 'test_cookie')

    def test_stats_init(self):
        self.database.register_user('test_user', 'test_cookie')
        user_raw = self.database.get_user_raw('test_cookie')

        self.assertEqual(user_raw[3], 0)
        self.assertEqual(user_raw[4], 0)
        self.assertEqual(user_raw[5], 0)
        self.assertEqual(user_raw[6], 0)
        self.assertEqual(user_raw[7], 0)

    def _stat_helper(self, func, params, index, second=None):
        self.database.register_user('test_user_1', 'test_cookie_1')
        self.database.register_user('test_user_2', 'test_cookie_2')

        func(params)

        user_raw_1 = self.database.get_user_raw('test_cookie_1')
        user_raw_2 = self.database.get_user_raw('test_cookie_2')

        self.assertEqual(user_raw_1[index], 1)
        self.assertEqual(user_raw_2[index], 0)

        if not second:
            return

        func(second)

        user_raw_1 = self.database.get_user_raw('test_cookie_1')
        user_raw_2 = self.database.get_user_raw('test_cookie_2')

        self.assertEqual(user_raw_1[index], 2)
        self.assertEqual(user_raw_2[index], 1)

    def test_add_game_completed(self):
        self._stat_helper(self.database.add_game_completed, ['test_cookie_1'], 3,
                          ['test_cookie_1', 'test_cookie_2'])

    def test_add_game_won(self):
        self._stat_helper(self.database.add_game_won, 'test_cookie_1', 4)

    def test_add_round_completed(self):
        self._stat_helper(self.database.add_round_completed, ['test_cookie_1'], 5,
                          ['test_cookie_1', 'test_cookie_2'])

    def test_add_round_judged(self):
        self._stat_helper(self.database.add_round_judged, 'test_cookie_1', 6)

    def test_add_round_won(self):
        self._stat_helper(self.database.add_round_won, 'test_cookie_1', 7)

    def tearDown(self):
        self.database._delete()


if __name__ == '__main__':
    main()
