import logging
import uuid

from random import randint

from config import conf

logging.basicConfig(level=logging.DEBUG, format='[Stor.io %(asctime)s] - %(message)s')

ERROR_MESSAGES = {
    '1': 'Username too long.',
    '2': 'Username already exists.',
    '3': 'Game does not exist.',
    '4': 'Game is full.',
    '5': 'You have been kicked due to inactivity.',
    '6': 'There are not enough players to continue the game.',
    '7': 'The admin has left the game.',
    '8': 'You have been kicked by the admin.'
}

ERRORS_CODES = {
    'USERNAME_TOO_LONG': '1',
    'USERNAME_EXISTS': '2',
    'GAME_DOES_NOT_EXIST': '3',
    'GAME_FULL': '4',
    'KICKED_INACTIVITY': '5',
    'NOT_ENOUGH_PLAYERS': '6',
    'ADMIN_LEFT': '7',
    'KICKED': '8'
}


def log(message):
    if conf['show_debug_messages']:
        logging.debug('[DEBUG] {}'.format(message))


def get_error_code(error):
    if error in ERRORS_CODES:
        return ERRORS_CODES[error]
    return '0'


def get_error_message(error_code):
    if error_code in ERROR_MESSAGES:
        return ERROR_MESSAGES[error_code]
    return 'Unknown Error'


def generate_session_cookie():
    # The call uuid.uuid4() generates a random string. Here is an example of what one looks like:
    # "fcab095b-6714-4122-a01d-f8cbebc4972f"
    return str(uuid.uuid4())


def generate_game_code():
    return str(randint(100000, 999999))


def get_user_if_good(request, database):
    cookie = request.cookies.get('session')
    if not cookie:
        return False

    user_raw = database.get_user_raw(cookie)
    if not user_raw:
        return False

    return {
        'id': user_raw[0],
        'name': user_raw[1],
        'cookie': user_raw[2],
        'games_completed': user_raw[3],
        'games_won': user_raw[4],
        'rounds_completed': user_raw[5],
        'rounds_judged': user_raw[6],
        'rounds_won': user_raw[7]
    }
