const network_status_element = document.getElementById('network-status');
const users_list_element = document.getElementById('users-list');
const choices_list_element = document.getElementById('choices-list');

const action_wrapper_element = document.getElementById('action-wrapper');
const action_element = document.getElementById('action');

const judge_wrapper_element = document.getElementById('judge-wrapper');
const judge_element = document.getElementById('judge');

const topic_wrapper_element = document.getElementById('topic-wrapper');
const topic_element = document.getElementById('topic');

const timer_wrapper_element = document.getElementById('timer-wrapper');
const timer_element = document.getElementById('timer');

const info_element = document.getElementById('info');
const username_element = document.getElementById('username');
const start_game_button_element = document.getElementById('start-game-button');

const submission_element = document.getElementById('submission');
const submission_length_element = document.getElementById('submission-length');
const submit_button_element = document.getElementById('submit-button');

const story_list_element = document.getElementById('story-list');
const story_wrapper_element = document.getElementById('story-wrapper');

const choices_wrapper_element = document.getElementById('choices-wrapper');
const submission_wrapper_element = document.getElementById('submission-wrapper');

let socket = io.connect('http://' + document.domain + ':' + location.port);
let timer;
let max_timer;
let ignore_requests = false;

setTimer(timer_element.innerText);

submit_button_element.onclick = function () {
    if (!submission_element.value) return;

    submit(submission_element.value);
    submission_element.value = '';
};

submission_element.onkeyup = function(){
    submission_length_element.innerText = submission_element.value.length.toString();

    // Prevent the user from entering a new line.
    submission_element.value = submission_element.value.replace(/\n/g, '');
};

if (start_game_button_element) {
    start_game_button_element.onclick = function() {
        start_game_button_element.className = 'hidden';
        socket.emit('start_game');
    }
}

function load_choices(choices, can_vote) {
    choices_wrapper_element.className = '';

    for (let choice of choices) {
        const child_element = document.createElement('p');
        child_element.innerText = '"' + choice + '"';

        if (can_vote) {
            const child_button_element = document.createElement('button');
            child_button_element.innerText = 'Vote';
            child_button_element.onclick = Function('submit("' + choice.replace(/"/g, '\\\"') + '")');
            child_element.appendChild(document.createTextNode('\u00A0'));
            child_element.appendChild(child_button_element);
        }

        choices_list_element.appendChild(child_element);
    }
}

function clear_choices() {
    choices_wrapper_element.className = 'hidden';

    while (choices_list_element.firstChild)
        choices_list_element.removeChild(choices_list_element.firstChild);
}

function submit(vote) {
    info_element.innerText = 'You submitted "' + vote + '".\nWait for the rest of the players.';

    socket.emit('submit', vote);
    clear_choices();
    submission_wrapper_element.className = 'hidden';
    submission_length_element.innerText = '0';
}

function kick(name) {
    socket.emit('kick', name);
}

function ban(name) {
    socket.emit('ban', name);
}

function increase_score(name) {
    const score_element = document.getElementById(name + '-score');
    score_element.innerText = parseInt(score_element.innerText) + 1;
}

function add_to_story(sentence) {
    const child_element = document.createElement('p');
    child_element.innerText = sentence;
    story_list_element.appendChild(child_element);
    story_list_element.parentElement.scrollTop = story_list_element.parentElement.scrollHeight;
}

function setTimer(time) {
    timer = time;
    max_timer = time;
}

function tick() {
    if (timer > 0) {
        timer--;
        timer_element.innerText = timer;
    }
    else if (timer === 0) {
        timer = -1;

        // We can check if the start game button exists to determine if we are the admin or not.
        if (start_game_button_element)
            socket.emit('close');
    }
}

socket.on('connect', function() {
    network_status_element.innerText = 'Connected';
});

socket.on('disconnect', function() {
    network_status_element.innerText = 'Disconnected';
});

socket.on('start', function(choices) {
    story_wrapper_element.className = '';
    timer_wrapper_element.className = '';
    info_element.innerText += 'Vote on a topic for the story!';
    load_choices(choices, true);

    timer = 60;
});

socket.on('start_round', function (judge, topic, choices) {
    judge_element.innerText = judge;
    judge_wrapper_element.className = '';
    action_wrapper_element.className = 'hidden';

    // Checking if the topic_element has text tells us if this is the first round or not.
    if (!topic_element.innerText) {
        topic_element.innerText = topic;
        topic_wrapper_element.className = '';
        info_element.innerText = '"' + topic + '" has been chosen as the topic!';
    }
    else info_element.innerText = '';

    let is_judge = judge === username_element.innerText;
    if (is_judge)
        info_element.innerText += '\nYou are the judge! Select an action for the round.';
    else
        info_element.innerText += '\nWait for the judge to select an action for the round.';

    clear_choices();
    load_choices(choices, is_judge);

    timer = 30;
});

socket.on('open_sentence_submissions', function(action) {
    clear_choices();

    action_wrapper_element.className = '';
    action_element.innerText = action;

    info_element.innerText = 'The action is "' + action + '".';

    if (judge_element.innerText === username_element.innerText)
        info_element.innerText += '\nYou are the judge!\nWait for players to submit their responses.';
    else {
        submission_wrapper_element.className = '';
        info_element.innerText += '\nWrite your sentence!';
    }

    timer = 90;
});

socket.on('open_judging', function (choices) {
    let is_judge = judge_element.innerText === username_element.innerText;

    if (is_judge)
        info_element.innerText = 'You are the judge!\nPick a sentence to add to the story.';
    else
        info_element.innerText = 'Wait for the judge to choose a sentence.';

    load_choices(choices, is_judge);

    timer = 90;
});

socket.on('end_round', function(winner, sentence) {
    increase_score(winner);
    add_to_story(sentence);

    clear_choices();
    judge_wrapper_element.className = 'hidden';
    info_element.innerText = winner + ' won the round with the sentence "' + sentence + '"!';
});

socket.on('end_game', function(winner, sentence) {
    increase_score(winner);
    add_to_story(sentence);
    clear_choices();

    judge_wrapper_element.className = 'hidden';
    timer_wrapper_element.className = 'hidden';
    action_wrapper_element.className = 'hidden';

    document.querySelectorAll('.player-button').forEach(function (element) {
        element.className = 'hidden';
    });

    info_element.innerText = winner + ' won the game with the sentence "' + sentence + '"!';
});

socket.on('user_join', function(name) {
    const child_element = document.createElement('p');
    child_element.id = name + '-wrapper';
    child_element.innerText = name + ' - ';

    const child_score_element = document.createElement('span');
    child_score_element.id = name + '-score';
    child_score_element.innerText = '0';
    child_element.appendChild(child_score_element);

    // We can check if the start game button exists to determine if we are the admin or not.
    if (start_game_button_element) {
        const kick_button_element = document.createElement('button');
        kick_button_element.innerText = 'Kick';
        kick_button_element.className = 'player-button';
        kick_button_element.onclick = Function('kick("' + name.replace(/"/g, '\\\"') + '")');
        child_element.appendChild(document.createTextNode('\u00A0'));
        child_element.appendChild(kick_button_element);

        const ban_button_element = document.createElement('button');
        ban_button_element.innerText = 'Ban';
        ban_button_element.className = 'player-button';
        ban_button_element.onclick = Function('ban("' + name.replace(/"/g, '\\\"') + '")');
        child_element.appendChild(document.createTextNode('\u00A0'));
        child_element.appendChild(ban_button_element);
    }

    users_list_element.appendChild(child_element);

    // Reset the timer so that all players have sufficient time.
    timer = max_timer;
});

socket.on('user_leave', function (name, error_code) {
    if (ignore_requests) return;

    // Check if the current user is the one being kicked.
    if (name === username_element.innerText)
        window.location.href = '/error?error_code=' + error_code;
    else {
        const user_wrapper = document.getElementById(name + '-wrapper');
        user_wrapper.parentElement.removeChild(user_wrapper);
    }
});

setInterval(tick, 1000);
